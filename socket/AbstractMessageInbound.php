<?php

namespace socket\AbstractMessageInbound;

abstract class AbstractMessageInbound
{

    public $listOfConnectedSocketClients;

    protected $usersConnected;

    abstract public function onOpen($SocketResource);

    abstract public function onClose(?string $client_ip_address, $socketResource);

    abstract public function onTextMessageOfClient(?string $socketData, $socketResource);

    abstract public function doHandshake($received_header, $client_socket_resource, ?string $host_name, int $port);

    public function onSendMessage($clientSocket, ?string $message)
    {

        $socket_write = socket_write($clientSocket, $message, strlen($message));

        /** @var resource $socket_write */
        if ($socket_write === false) {
            $errorCode = socket_last_error();
            $errorMsg = socket_strerror($errorCode);
            throw new \Exception("No se pudo envíar mensaje al socket: [$errorCode] $errorMsg", 1);
        }
    }

}
