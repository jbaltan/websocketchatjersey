<?php

namespace socket\ChatHandler;

use app\php\Common\Entities\ResponseData\ResponseData;
use app\php\Service\ServiceFactory\ServiceFactory;

use Exception;

use socket\AbstractMessageInbound\AbstractMessageInbound;

use app\php\Common\Entities\DataSocket\DataSocket;
use app\php\Common\Entities\TypesResponseSocket\TypesResponseSocket;
use app\php\Common\Entities\TypesRequestSocket\TypesRequestSocket;
use app\php\Common\Entities\ResponseWebSocket\ResponseWebSocket;

use jerseyPhp\utils\Json\Json;

class ChatHandler extends AbstractMessageInbound
{

    /**
     * @param string|null $socketMessage
     * @param $socketResource
     */
	public function onTextMessageOfClient(?string $socketMessage, $socketResource)
	{

		if (!empty($socketMessage)) {

			try {

				$dataSocket = Json::convertObjectJsonToObjectPhp($socketMessage, "app\php\Common\Entities\DataSocket\DataSocket");

				if (!empty($dataSocket) && $dataSocket instanceof DataSocket) {
				    $dataSocket->setSocket($socketResource);
				    $this->onProcessMessage($dataSocket);
                }

			} catch (Exception $e) {
				//error al obtener el objeto
			}
		}
	}

	private function onProcessMessage(?DataSocket $dataSocket){

		if (!empty($dataSocket)) {

			//Se valida si el socket es una newConnection, para posteriormente en la lista de usersConnected actualizar el idUserCurrent
			if ($dataSocket->getType() == TypesRequestSocket::newConnection) {

				foreach ($this->usersConnected as $userConnected) {
					if ($userConnected->getSocket() == $dataSocket->getSocket()) {
                        $userConnected->setIdUserCurrent($dataSocket->getIdUserCurrent());
						break;
					}
				}
			} else if ($dataSocket->getType() == TypesRequestSocket::messageRead) {

                $responseData = ServiceFactory::getChatService($dataSocket->getCodeBd())->updateReadMessages($dataSocket->getMessage()->getChat()->getId());

                if ($responseData->isError()){

                }else{
                    //Se ubica el socket del usuario Origen del Mensaje al cual se Notifica que su mensaje fue visto
                    foreach ($this->usersConnected as $userConnected) {

                        if ($dataSocket->getMessage()->getUserOrigen()->getId() == $userConnected->getIdUserCurrent()) {

                            $responseWebSocket = new ResponseWebSocket(TypesResponseSocket::destinationMessageRead, null);

                            try {
                                $this->onSendMessage($userConnected->getSocket(), $this->seal(json_encode($responseWebSocket)));
                            } catch (Exception $exception) {

                            }
                            break;
                        }
                    }
                }

            } else if ($dataSocket->getType() == TypesRequestSocket::newMessage) {

                if (!empty($dataSocket->getMessage())) {

                        //para validar si el usuario destino está conectado o no
                        $foundIt = false;

                        $dataSocket->getMessage()->setFecha(date('Y-m-d H:i:s'));

                        foreach ($this->usersConnected as $userConnected) {

                            //Se busca el socket del usuario destino
                            if ($dataSocket->getMessage()->getUserDestino()->getId() == $userConnected->getIdUserCurrent()) {

                                $type = TypesResponseSocket::messageSend;
                                if (empty($dataSocket->getMessage()->getChat()->getId())) {
                                    $type = TypesResponseSocket::newChat;
                                }

                                //se manda a guardar el mensaje a MongoDb, en su interior válida si es un nuevo Chat
                                $responseData = $this->goToSaveMessage($dataSocket, $userConnected->getIdUserCurrent());

                                $foundIt = true;

                                if ($responseData->isError()) {
                                    //Se notifica a quien envío el mensaje, error al intentar envíar mensaje(guardar mensaje)
                                    $message = $this->seal(json_encode(new ResponseWebSocket(TypesResponseSocket::error, $responseData)));

                                    try {
                                        $this->onSendMessage($dataSocket->getSocket(), $message);
                                    } catch (Exception $e) {
                                    }

                                } else {

                                    try {
                                        //Se notifica a quien recibe el mensaje
                                        $responseWebSocket = $this->seal(json_encode(new ResponseWebSocket(TypesResponseSocket::newMessage, $responseData->getInfo())));
                                        $this->onSendMessage($userConnected->getSocket(), $responseWebSocket);

                                        //Se notifica a quien envío el mensaje, de que su mensaje ha sido recibido
                                        $responseWebSocket = $this->seal(json_encode(new ResponseWebSocket($type, $responseData->getInfo())));
                                        $this->onSendMessage($dataSocket->getSocket(), $responseWebSocket);

                                    } catch (Exception $e) {
                                    }

                                }

                                break;
                            }
                        }

                        //Si el usuario destino no esta entro los usuarios conectados se debe guardar en BD
                        if (!$foundIt) {

                            //print_r($dataSocket);
                            $responseData = $this->goToSaveMessage($dataSocket, $dataSocket->getMessage()->getUserDestino()->getId());
                            if ($responseData->isError()) {
                                //errores
                                try {
                                    $this->onSendMessage($dataSocket->getSocket(), $this->seal(json_encode(new ResponseWebSocket(TypesResponseSocket::error, $responseData))));
                                } catch (Exception $e) {
                                }
                            } else {
                                //se notifica a quien envío el mensaje
                                try {
                                    $this->onSendMessage($dataSocket->getSocket(), $this->seal(json_encode(new ResponseWebSocket(TypesResponseSocket::messageSend, $dataSocket->getMessage()))));
                                } catch (Exception $e) {
                                }
                            }
                        }
                    }
            }
		}

	}

	private function goToSaveMessage(?DataSocket $dataSocket, ?int $idUserConnected): ?ResponseData
    {

        if (empty($dataSocket->getMessage()->getChat()->getId())) {

			//Creation de Chat in MongoDb
			$responseData = ServiceFactory::getChatService($dataSocket->getCodeBd())->onCreateChat(
                $dataSocket->getMessage()->getUserOrigen()->getId(),
                $dataSocket->getMessage()->getUserDestino()->getId()
            );

            if (!$responseData->isError()) {
                $dataSocket->getMessage()->getChat()->setId($responseData->getInfo());
            }
		}

		return ServiceFactory::getChatService($dataSocket->getCodeBd())->onSaveMessage(
			$dataSocket->getMessage()->getChat()->getId(),
			$dataSocket->getMessage()->getUserOrigen()->getId(),
            $idUserConnected,
			$dataSocket->getMessage()
		);
	}

	private function seal(?string $socketData):?string {
		$b1 = 0x80 | (0x1 & 0x0f);
		$length = strlen($socketData);

        $header = null;

		if ($length <= 125)
			$header = pack('CC', $b1, $length);
		elseif ($length < 65536)
			$header = pack('CCn', $b1, 126, $length);
		else
			$header = pack('CCNN', $b1, 127, $length);
		return $header . $socketData;
	}

	public function doHandshake($received_header, $client_socket_resource, $host_name, $port)
	{

		$headers = array();
		$lines = preg_split("/\r\n/", $received_header);
		foreach ($lines as $line) {
			$line = chop($line);
			if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
				$headers[$matches[1]] = $matches[2];
			}
		}

		$secKey = $headers['Sec-WebSocket-Key'];
		$secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
		$buffer  = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
			"Upgrade: websocket\r\n" .
			"Connection: Upgrade\r\n" .
			"WebSocket-Origin: $host_name\r\n" .
			"WebSocket-Location: ws://$host_name:$port/demo/shout.php\r\n" .
			"Sec-WebSocket-Accept:$secAccept\r\n\r\n";

		try {
			//Headers de response at client for connection
			$this->onSendMessage($client_socket_resource, $buffer);
		} catch (Exception $exception) {

		}
	}

	/**
	 * @param mixed $SocketResource
	 * Se adiciona a la lista de conexiones nuevo cliente socket, posteriormente al metodo onProcessMessage
	 * se valida si el mensaje es una nueva conexion y buscarlo en usuariosConectados  y setear info del usuario current
	 */
	public function onOpen($SocketResource)
	{

		$dataSocket = new DataSocket();
		$dataSocket->setSocket($SocketResource);
		$this->usersConnected[] = $dataSocket;

	}

	public function onClose($client_ip_address, $socketResource)
	{

		//Se recibe el recurso socket desconectado, se busca en las listas de conexiones para eliminarlo

		for ($i = 0; $i < count($this->usersConnected); $i++) {
			if ($this->usersConnected[$i]->getSocket() == $socketResource) {

				unset($this->usersConnected[$i]);
				$this->usersConnected = array_values($this->usersConnected);

				unset($this->listOfConnectedSocketClients[$i + 1]);
				$this->listOfConnectedSocketClients = array_values($this->listOfConnectedSocketClients);

				break;
			}
		}

	}

}
