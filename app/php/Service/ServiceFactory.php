<?php

namespace app\php\Service\ServiceFactory;

use app\php\Service\ChatService\ChatService;
use app\php\Service\IChatService\IChatService;
use app\php\Common\dao\MongoDb\ChatDaoMongoDb\ChatDaoMongoDb;

class ServiceFactory
{

    public static function getChatService(?string $codeBd): IChatService
    {

        $service = new ChatService();
        $service->setChatDao(new ChatDaoMongoDb($codeBd));
        return $service;
    }
}
