<?php

namespace app\php\Service\IChatService;

use app\php\Common\Entities\ResponseData\ResponseData;
use app\php\Common\Entities\Message\Message;

interface IChatService
{

    /**
     * @param int $idUserOrigin
     * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function onCreateChat(int $idUserOrigin, int $idUserDestine): ?ResponseData;

    /**
     * @param string $idChat
     * @param int $idUserOrigin
     * @param int $idUserDestine
     * @param Message $message
     * @return ResponseData|null
     */
    public function onSaveMessage(string $idChat, int $idUserOrigin, int $idUserDestine, Message $message): ?ResponseData;

    /**
     * @param int $idUser
     * @return ResponseData|null
     */
    public function getChatsAndMessagesByUser(int $idUser): ?ResponseData;

    /**
     * @param string $idChat
     * @return ResponseData|null
     */
    public function updateReadMessages(string $idChat): ?ResponseData;

    /**
     * @param int $idUserOrigin
     * * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function getChatByUserOriginAndUserDestine(int $idUserOrigin, int $idUserDestine);
}
