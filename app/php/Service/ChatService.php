<?php

namespace app\php\Service\ChatService;

use app\php\Service\IChatService\IChatService;

use app\php\Common\dao\MongoDb\IChatDaoMongoDb\IChatDaoMongoDb;

use app\php\Common\Entities\ResponseData\ResponseData;
use app\php\Common\Entities\Message\Message;

/**
 * @author julian99
 *
 */
class ChatService implements IChatService
{

    /**
     * @var
     */
    private $chatDao;

    /**
     * @param int $idUserOrigin
     * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function onCreateChat(int $idUserOrigin, int $idUserDestine): ?ResponseData
    {
        return $this->getChatDao()->onCreateChat($idUserOrigin, $idUserDestine);
    }

    /**
     * @param string $idChat
     * @param int $idUserOrigin
     * @param int $idUserDestine
     * @param Message $message
     * @return ResponseData|null
     */
    public function onSaveMessage(string $idChat, int $idUserOrigin, int $idUserDestine, Message $message): ?ResponseData
    {
        return $this->getChatDao()->onSaveMessage($idChat, $idUserOrigin, $idUserDestine, $message);
    }

    /**
     * @param int $idUser
     * @return ResponseData|null
     */
    public function getChatsAndMessagesByUser(int $idUser): ?ResponseData
    {
        return $this->getChatDao()->getChatsAndMessagesByUser($idUser);
    }

    /**
     * @param string $idChat
     * @return ResponseData|null
     */
    public function updateReadMessages(string $idChat): ?ResponseData
    {
        return $this->getChatDao()->updateReadMessages($idChat);
    }

    /**
     * @param int $idUserOrigin
     * * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function getChatByUserOriginAndUserDestine(int $idUserOrigin, int $idUserDestine): ?ResponseData
    {
        return $this->getChatDao()->getChatByUserOriginAndUserDestine($idUserOrigin, $idUserDestine);
    }

    /**
     * @return IChatDaoMongoDb
     */
    public function getChatDao(): IChatDaoMongoDb
    {
        return $this->chatDao;
    }

    /**
     * @param IChatDaoMongoDb $chatDao
     */
    public function setChatDao(IChatDaoMongoDb $chatDao)
    {
        $this->chatDao = $chatDao;
    }
}
