<?php

namespace app\php\Common\Config;

/**
 * Esta class lee el file xml de settings
 */
class Config
{

	public static function readXml()
	{
		return simplexml_load_file(conf);
	}

	public static function readXmlCodesBd()
    {
		return simplexml_load_file(confCodesBd);
	}
}
