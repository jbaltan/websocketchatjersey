<?php

namespace app\php\Common\Entities\Chat;

use app\php\Common\Entities\User\User;
use jerseyPhp\utils\ArrayList\ArrayList;
use JsonSerializable;

class Chat implements JsonSerializable
{

    private $id;

    private $userOrigen;
    private $userDestino;

    private $messages;

    /**
     * @return ArrayList|null
     */
    public function getMessages(){
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id)
    {
        $this->id = $id;
    }

    /**
     * @return User|null
     */
    public function getUserOrigen(): ?User
    {
        return $this->userOrigen;
    }

    /**
     * @param User|null $userOrigen
     */
    public function setUserOrigen(?User $userOrigen)
    {
        $this->userOrigen = $userOrigen;
    }

    /**
     * @return mixed
     */
    public function getUserDestino(): ?User
    {
        return $this->userDestino;
    }

    /**
     * @param User|null $userDestino
     */
    public function setUserDestino(?User $userDestino)
    {
        $this->userDestino = $userDestino;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
