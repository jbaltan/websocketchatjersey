<?php

namespace app\php\Common\Entities\User;

class User implements \JsonSerializable
{

    private $id;

    private $nombre;
    private $descripcion;


    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre(?string $nombre){
        $this->nombre = $nombre;
    }

    
    /**
     * @return mixed
     */
    public function getDescripcion():?String{
        return $this->descripcion;
    }
    
    /**
     * @param mixed $descripcion
     */
    public function setDescripcion(?String $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
