<?php

namespace app\php\Common\Entities\TypesRequestSocket;

abstract class TypesRequestSocket
{
    const newConnection = 1;
    const newMessage = 2;
    const messageRead = 3;

}
