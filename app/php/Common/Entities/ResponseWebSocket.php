<?php

namespace app\php\Common\Entities\ResponseWebSocket;

class ResponseWebSocket implements \JsonSerializable
{

    private $type;

    private $info;

    public function __construct(int $type, $info)
    {
        $this->type = $type;
        $this->info = $info;
    }

    /**
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @return void
     */
    public function setType(?int $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param void
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
