<?php

namespace app\php\Common\Entities\TypesResponseSocket;

abstract class TypesResponseSocket
{
    const messageSend = 1;
    const newMessage = 2;
    const destinationMessageRead = 3;
    const error = 4;
    const newChat = 5;
}
