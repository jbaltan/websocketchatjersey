<?php

namespace app\php\Common\Entities\DataSocket;

use app\php\Common\Entities\Message\Message;
use JsonSerializable;

class DataSocket implements JsonSerializable
{

    private $socket;

    private $idUserCurrent = 0;

    private $type;

    private $message;
    private $codeBd;


    /**
     * @return mixed
     */
    public function getSocket()
    {
        return $this->socket;
    }

    /**
     * @param mixed $socket
     */
    public function setSocket($socket)
    {
        $this->socket = $socket;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     * @return void
     */
    public function setType(?int $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getIdUserCurrent(): ?int
    {
        return $this->idUserCurrent;
    }

    /**
     *  * @param int|null $idUserCurrent
     * @return void
     */
    public function setIdUserCurrent(?int $idUserCurrent)
    {
        $this->idUserCurrent = $idUserCurrent;
    }

    /**
     * 
     * @return Message|null
     */
    public function getMessage(): ?Message
    {
        return $this->message;
    }

    /**
     * 
     * @param Message|null $message
     * @return void
     */
    public function setMessage(?Message $message)
    {
        $this->message = $message;
    }

    /**
     * 
     * @return string|null
     */
    public function getCodeBd(): ?string
    {
        return $this->codeBd;
    }

    /**
     * 
     * @param string|null $codeBd
     * @return void
     */
    public function setCodeBd(?string $codeBd)
    {
        $this->codeBd = $codeBd;
    }

    public function jsonSerialize()
    {
        $get_object_vars = get_object_vars($this);
        $get_object_vars["socket"] = null;
        return $get_object_vars;
    }
}
