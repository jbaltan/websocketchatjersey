<?php

namespace app\php\Common\Entities\Message;

use app\php\Common\Entities\Chat as Chat;
use app\php\Common\Entities\User as User;

class Message implements \JsonSerializable
{

    private $id;

    private $message;

    private $userOrigen;
    private $userDestino;

    private $chat;

    private $fecha;

    private $isRead;

    /**
     * @return mixed
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(?string $id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage(?string $message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getChat(): ?Chat\Chat
    {
        return $this->chat;
    }


    /**
     * @param mixed $chat
     */
    public function setChat(?Chat\Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * @return mixed
     */
    public function getUserOrigen(): ?User\User
    {
        return $this->userOrigen;
    }

    /**
     * @param mixed $userOrigen
     */
    public function setUserOrigen(?User\User $userOrigen)
    {
        $this->userOrigen = $userOrigen;
    }

    /**
     * @return mixed
     */
    public function getUserDestino(): ?User\User
    {
        return $this->userDestino;
    }

    /**
     * @param mixed $userDestino
     */
    public function setUserDestino(?User\User $userDestino)
    {
        $this->userDestino = $userDestino;
    }

    /**
     * @return mixed
     */
    public function getIsRead(): ?bool
    {
        return $this->isRead;
    }

    /**
     * @param mixed $isRead
     */
    public function setIsRead(?bool $isRead)
    {
        $this->isRead = $isRead;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $isRead
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
