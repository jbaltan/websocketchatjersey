<?php

namespace app\php\Common\Entities\ResponseData;

use JsonSerializable;

class ResponseData implements JsonSerializable{

    private $error;
    private $message;
    private $info;
    
    public function __construct(bool $error, string $message, $info){
        $this->error = $error;
        $this->message = $message;
        $this->info = $info;
    }

    /**
     * @return bool
     */
    public function isError():?bool{
        return $this->error;
    }

    /**
     * @return string
     */
    public function getMessage():?string{
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getInfo(){
        return $this->info;
    }

    /**
     * @param bool $error
     */
    public function setError(?bool $error){
        $this->error = $error;
    }

    /**
     * @param string $message
     */
    public function setMessage(?string $message){
        $this->message = $message;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }    
    
    public function jsonSerialize(){
        return get_object_vars($this);
    }

}
