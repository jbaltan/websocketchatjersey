<?php

namespace app\php\Common\dao\MongoDb\ChatDaoMongoDb;

use Exception;
use app\php\Common\dao\MongoDb\{IChatDaoMongoDb as IChatDaoMongoDb,
    ConnectionClientMongoDb
};

use app\php\Common\Entities\Chat\Chat;
use app\php\Common\Entities\User\User;
use app\php\Common\Entities\Message\Message;
use app\php\Common\Entities\ResponseData\ResponseData;

use jerseyPhp\utils\ArrayList\ArrayList;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Exception\Exception as ExceptionMongoDb;
use MongoDB\Driver\Query;

class ChatDaoMongoDb extends ConnectionClientMongoDb\ConnectionClientMongoDb implements IChatDaoMongoDb\IChatDaoMongoDb
{

    public function __construct(?string $codeBd)
    {
        parent::__construct($codeBd);
    }

    public function onCreateChat(int $idUserOrigin, int $idUserDestine): ?ResponseData
    {

        $data = new ResponseData(false, "", 0);

        try {

            $client = $this->getClient();

            $bulkWrite = new BulkWrite();
            $data->setInfo($bulkWrite->insert(['userOrigen' => $idUserOrigin, 'userDestino' => $idUserDestine]));

            $client->executeBulkWrite($this->getNameBdByCode().'.chats', $bulkWrite);
        } catch (Exception $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        }

        return $data;
    }

    public function onSaveMessage(string $idChat, int $idUserOrigin, int $idUserDestine, Message $message): ?ResponseData
    {

        $data = new ResponseData(false, "", 0);

        try {

            $client = $this->getClient();

            $bulkWrite = new BulkWrite();
            $message->setId($bulkWrite->insert(['idChat' => $idChat, 'userOrigen' => $idUserOrigin, 'userDestino' => $idUserDestine, 'message' => $message->getMessage(), 'isRead' => false, 'fecha' => date('Y-m-d H:i:s')]));

            $client->executeBulkWrite($this->getNameBdByCode().'.messages', $bulkWrite);
            $data->setInfo($message);
        } catch (Exception $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        }

        return $data;
    }

    public function updateReadMessages(string $idChat): ?ResponseData
    {

        $data = new ResponseData(false, "", 0);

        try {

            $client = $this->getClient();

            $bulkWrite = new BulkWrite();

            $filter = ['idChat' => $idChat];
            $update = ['$set' => ['isRead' => true]];
            $options = ['multi' => true, 'upsert' => false];
            $bulkWrite->update($filter, $update, $options);
            $client->executeBulkWrite($this->getNameBdByCode().'.messages', $bulkWrite);
        } catch (Exception $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        }
        return $data;
    }

    public function getChatsAndMessagesByUser(int $idUser): ?ResponseData
    {

        $listChats = new ArrayList();

        $data = new ResponseData(false, "", $listChats);

        try {

            $client = $this->getClient();

            $query = new Query(['userOrigen' => $idUser], []);

            $documentsFist = $client->executeQuery($this->getNameBdByCode() . '.chats', $query)->toArray();

            foreach ($documentsFist as $document) {

                $messages = $client->executeQuery($this->getNameBdByCode().'.messages', new Query(['idChat' => $document->_id->__toString()], []))->toArray();

                $document->messages = $messages;
            }

            $query = new Query(['userDestino' => $idUser], []);
            $documentsSecond = $client->executeQuery($this->getNameBdByCode() . '.chats', $query)->toArray();

            foreach ($documentsSecond as $document) {

                $messages = $client->executeQuery($this->getNameBdByCode().'.messages', new Query(['idChat' => $document->_id->__toString()], []))->toArray();

                $document->messages = $messages;
            }

            $rows = array_merge($documentsFist, $documentsSecond);

            foreach ($rows as $row) {

                $chatPreExist = new Chat();
                $chatPreExist->setId($row->_id->__toString());

                $userOrigin = new User();
                $userOrigin->setId($row->userOrigen);

                $userDestino = new User();
                $userDestino->setId($row->userDestino);

                $chatPreExist->setUserDestino($userDestino);
                $chatPreExist->setUserOrigen($userOrigin);

                $listMessages = new ArrayList();

                foreach ($row->messages as $messageMongodb) {

                    $message = new Message();
                    $message->setId($messageMongodb->_id->__toString());
                    $message->setMessage($messageMongodb->message);
                    $message->setFecha($messageMongodb->fecha);
                    $message->setIsRead($messageMongodb->isRead);

                    $userOrigenMessage = new User();
                    $userOrigenMessage->setId($messageMongodb->userOrigen);

                    $message->setUserOrigen($userOrigenMessage);

                    $userDestinoMessage = new User();
                    $userDestinoMessage->setId($messageMongodb->userDestino);

                    $message->setUserDestino($userDestinoMessage);

                    $chatMessage = new Chat();
                    $chatMessage->setId($chatPreExist->getId());
                    $chatMessage->setUserOrigen($chatPreExist->getUserOrigen());
                    $chatMessage->setUserDestino($chatPreExist->getUserOrigen());

                    $message->setChat($chatMessage);

                    $listMessages->add($message);
                }

                $chatPreExist->setMessages($listMessages);

                $listChats->add($chatPreExist);
            }
        } catch (ExceptionMongoDb $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        } catch (Exception $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        }

        return $data;
    }

    /**
     * @param int $idUserOrigin
     * * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function getChatByUserOriginAndUserDestine(int $idUserOrigin, int $idUserDestine): ?ResponseData
    {
        $data = new ResponseData(false, "", null);

        try {

            $client = $this->getClient();

            $query = new Query(['userOrigen' => $idUserOrigin, 'userDestino' => $idUserDestine], []);

            $documentsFist = $client->executeQuery($this->getNameBdByCode() . '.chats', $query)->toArray();

            foreach ($documentsFist as $chatDocument) {
                $chatDocument->messages = $client->executeQuery($this->getNameBdByCode().'.messages', new Query(['idChat' => $chatDocument->_id->__toString()], []))->toArray();
            }

            foreach ($documentsFist as $row) {

                $chatUser = new Chat();
                $chatUser->setId($row->_id->__toString());

                $userOrigin = new User();
                $userOrigin->setId($row->userOrigen);

                $userDestino = new User();
                $userDestino->setId($row->userDestino);

                $chatUser->setUserDestino($userDestino);
                $chatUser->setUserOrigen($userOrigin);

                $listMessages = new ArrayList();

                foreach ($row->messages as $messageMongodb) {

                    $message = new Message();
                    $message->setId($messageMongodb->_id->__toString());
                    $message->setMessage($messageMongodb->message);
                    $message->setFecha($messageMongodb->fecha);
                    $message->setIsRead($messageMongodb->isRead);

                    $userOrigenMessage = new User();
                    $userOrigenMessage->setId($messageMongodb->userOrigen);

                    $message->setUserOrigen($userOrigenMessage);

                    $userDestinoMessage = new User();
                    $userDestinoMessage->setId($messageMongodb->userDestino);

                    $message->setUserDestino($userDestinoMessage);

                    $chatMessage = new Chat();
                    $chatMessage->setId($chatUser->getId());
                    $chatMessage->setUserOrigen($chatUser->getUserOrigen());
                    $chatMessage->setUserDestino($chatUser->getUserOrigen());

                    $message->setChat($chatMessage);

                    $listMessages->add($message);
                }

                $chatUser->setMessages($listMessages);
                $data->setInfo($chatUser);

            }
        } catch (ExceptionMongoDb $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        } catch (Exception $e) {
            $data->setError(true);
            $data->setMessage($e->getMessage());
        }

        return $data;
    }
}
