<?php

namespace app\php\Common\dao\MongoDb\ConnectionClientMongoDb;

use app\php\Common\Config\Config;
use MongoDB\Driver\Manager;

class ConnectionClientMongoDb
{

    private $codeBd;

    public function __construct(?string $codeBd)
    {
        $this->codeBd = $codeBd;
    }

    protected function getClient(): ?Manager
    {

        $info = Config::readXml();

        return new Manager("mongodb://" . $info->user . ":" . $info->pass . "@" . $info->host . ":" . $info->port . "/admin");
    }

    protected function getNameBdByCode(): ?string
    {
        $codes = Config::readXmlCodesBd();

        $items = $codes->item;

        foreach ($items as $item) {
            if ($this->codeBd == $item->code) {
                return $item->bd;
            }
        }
        return "";
    }
}
