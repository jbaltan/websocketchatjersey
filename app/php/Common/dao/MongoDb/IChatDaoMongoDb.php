<?php

namespace app\php\Common\dao\MongoDb\IChatDaoMongoDb;

use app\php\Common\Entities\ResponseData\ResponseData;
use app\php\Common\Entities\Message\Message;

interface IChatDaoMongoDb
{

    /**
     * @param int $idUserOrigin
     * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function onCreateChat(int $idUserOrigin, int $idUserDestine): ?ResponseData;

    public function onSaveMessage(string $idChat, int $idUserOrigin, int $idUserDestine, Message $message): ?ResponseData;

    public function getChatsAndMessagesByUser(int $idUser): ?ResponseData;

    public function updateReadMessages(string $idChat): ?ResponseData;

    /**
     * @param int $idUserOrigin
     * * @param int $idUserDestine
     * @return ResponseData|null
     */
    public function getChatByUserOriginAndUserDestine(int $idUserOrigin, int $idUserDestine): ?ResponseData;
}
