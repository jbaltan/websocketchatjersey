<?php

namespace app\php\Controls\ChatController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Content-Type: application/json');

use app\php\Service\ServiceFactory\ServiceFactory;

use jerseyPhp\Http\Response\Response;

class ChatController
{

    public function __construct($path = "chat")
    {
    }

    public function getChatsAndMessagesByUser(int $idUser, string $codeBd, $endPoint = "chatsAndMessagesByUser"): Response
    {
        $response = new Response();
        $response->setContent(json_encode(ServiceFactory::getChatService($codeBd)->getChatsAndMessagesByUser($idUser)));
        return $response;
    }

    public function getChatByUserOriginAndUserDestine(int $idUserOrigin, int $idUserDestine, string $codeBd, $endPoint = "chatByUserOriginAndUserDestine")
    {

        $response = new Response();
        $response->setContent(json_encode(ServiceFactory::getChatService($codeBd)->getChatByUserOriginAndUserDestine($idUserOrigin, $idUserDestine)));
        return $response;
    }

}
