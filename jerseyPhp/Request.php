<?php

declare(strict_types=1);

namespace jerseyPhp\Request;

use Exception;
use jerseyPhp\Config\Routes as Routes;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

require_once('Config/Routes.php');
require_once('Http/Response.php');
require_once('utils/ArrayList.php');
require_once('utils/HashMap.php');

class Request
{

    /**
     * @throws ReflectionException
     * @throws Exception
     */
    public static function process(string $path, string $endpoint)
    {

        $routes = Routes\Routes::getRoutes();
        $status = false;

        foreach ($routes as $route) {


            $split = explode('\\', $route['class']);
            unset($split[count($split) - 1]);

            require_once(implode("\\", $split) . ".php");

            $reflectedClass = (new Request)->getReflectionClassByNameStringClass($route['class']);

            if (null != $reflectedClass) {

                if (count($reflectedClass->getConstructor()->getParameters()) > 0) {
                    $pathClass = $reflectedClass->getConstructor()->getParameters()[0]->getDefaultValue();
                    $methods = $reflectedClass->getMethods();

                    foreach ($methods as $method) {
                        $reflectionMethods = new ReflectionMethod($route['class'], $method->name);
                        $parameters = $reflectionMethods->getParameters();

                        foreach ($parameters as $parameter) if ($pathClass == $path) {

                            if ($parameter->isDefaultValueAvailable()) {

                                if ($endpoint == strtolower($parameter->getDefaultValue())) {

                                    $params = [];
                                    $params[0] = null;
                                    $i = 0;

                                    foreach ($parameters as $parameter2) {
                                        if ($i < count($parameters)-1) {
                                            if (isset($_REQUEST[$parameter2->name])) {
                                                $params[$i] = $_REQUEST[$parameter2->name];
                                            } else {
                                                throw new Exception("Error Processing Request 3, param: " . $parameter2->name . " no encontrado", 1);
                                            }
                                        }
                                        $i++;
                                    }

                                    $response = $reflectionMethods->invokeArgs(new $route['class'](), $params);

                                    print $response->getContent();

                                    $status = true;
                                    break;
                                }
                            }
                        }

                        if ($status) {
                            break;
                        }
                    }
                }

                if ($status) break;
            } else throw new Exception("Error Processing Request", 1);

        }

        if (!$status) throw new Exception("Error Processing Request", 1);
        unset($_GET);
        unset($_POST);
    }

    /**
     * @param string|null $nameClass
     * @return ReflectionClass|null
     */
    private function getReflectionClassByNameStringClass(?string $nameClass): ?ReflectionClass
    {
        try {
            return new ReflectionClass($nameClass);
        } catch (ReflectionException $e) {
        }
        return null;
    }
}
