<?php

namespace jerseyPhp\Config\Routes;

class Routes
{

    public static function getRoutes(): array
    {
        return json_decode(file_get_contents("app/routesClassEndPoints.json"), true);
    }
}
