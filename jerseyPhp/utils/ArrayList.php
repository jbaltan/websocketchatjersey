<?php

namespace jerseyPhp\utils\ArrayList;

class ArrayList implements \JsonSerializable{

	private $arrayValue = array();
    private $arrayKey = array();

    public function get(int $key){
        $hash = self::getHash($key);
        $hashValue = $this->arrayValue[$hash];
        return $hashValue;
    }

    public function add($value){

        if(count($this->getAllKeys())==0){
            $key=count($this->getAllKeys());
        }else{
            $key =count($this->getAllKeys());
        }

        $hash = self::getHash($key);
        $this->arrayKey[$hash] = $key;
        $this->arrayValue[$hash] = $value;
    }

    private function getAllKeys(){
        return array_values($this->arrayKey);
    }

    public function getAllValues():array{
        return array_values($this->arrayValue);
    }

    public function size():int{
        return count($this->arrayKey);
    }

    private static function getHash(int $key){
        if(is_object($key)){
            return spl_object_hash($key);
        }
        return $key;
    }

    public function set(int $key, $value){
        $hash = self::getHash($key);
        $this->arrayValue[$hash] = $value;
    }
    
    public function jsonSerialize(){
        
        $arreglo = array();
        for ($i=0; $i <$this->size(); $i++) {
            $arreglo[$i] = $this->get($i);
        }
        return $arreglo;
        
    }

}

?>