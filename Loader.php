<?php

use jerseyPhp\Request\Request;

use app\php\Common\Entities\ResponseData as ResponseData;

use socket\Socket\Socket;
use socket\ChatHandler\ChatHandler;

class Loader
{

    public function __construct()
    {
        if (isset($_SERVER['argv'])) {
            if (!defined('conf')){
                define('conf', str_replace("Loader.php", "", $_SERVER['PHP_SELF']) . "app/WEB-APP/web.xml");
            }
            if (!defined('confCodesBd')){
                define('confCodesBd', str_replace("Loader.php", "", $_SERVER['PHP_SELF']) . "app/WEB-APP/codeBd.xml");
            }
        } else {
            if (!defined('conf')) {
                define('conf', "app/WEB-APP/web.xml");
            }
            if (!defined('confCodesBd')){
                define('confCodesBd', "app/WEB-APP/codeBd.xml");
            }
        }
    }

    static private function register(): void {
        spl_autoload_register(__NAMESPACE__ . '\Loader::autoLoad');
    }

    /**
     * @throws Exception
     */
    static private function autoLoad($nameClass): void {

        //print "$nameClass \n";

        $nameFile = str_replace("\\", "/", $nameClass) . ".php";

        $split = explode('/', $nameFile);

        $discount = count($split) >= 2 ? 2 : 1;
        
        $nameFile  = str_replace($split[count($split) - $discount] . "/", "", $nameFile);

        $nameClassTmp = str_replace(".php", "", $split[count($split) - 1]);

        $isNativeClass = false;

        //if (empty(Loader::$classNatives)){
            //Loader::$classNatives = get_declared_classes();
        //}

        //if (in_array($nameClassTmp, Loader::$classNatives)) {
            $isNativeClass = true;
        //}

        //if (!$isNativeClass){
            if (isset($_SERVER['argv'])) {
                //socket
                $php_self = str_replace("Loader.php", "", $_SERVER['PHP_SELF']);
                $nameFile = str_replace("\\", "/", $php_self).$nameFile;
            }

            if (file_exists($nameFile)){
                require_once $nameFile;
            }else {
                throw new Exception("Imposible importar la clase: $nameClass");
            }
        //}

    }

    public function load(): void{

        date_default_timezone_set("America/Bogota");

        Loader::register();

        if (isset($_SERVER['argv'])) {
            new Socket(new ChatHandler());
        } else {

            try {
                Request::process($_GET['path'], $_GET['endpoint']);
            } catch (Exception $e) {
                print json_encode(new ResponseData\ResponseData(true, $e->getMessage(), null));
            }
        }
    }
}

$loader = new Loader();
$loader->load();
